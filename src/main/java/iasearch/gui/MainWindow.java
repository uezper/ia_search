/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package iasearch.gui;

import iasearch.gui.table.ColumnGroup;
import iasearch.gui.table.GroupableTableHeader;
import iasearch.puzzle.Puzzle;
import iasearch.search.AStarSearch.Heuristic;
import iasearch.search.SearchType;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

/**
 * 
 * @author Uriel Pereira
 */
public class MainWindow extends javax.swing.JFrame {

    private static final int SIZE = 0;
    private static final int TIME_BA = 1;
    private static final int TIME_PI = 2;
    private static final int TIME_DM = 3;
    private static final int NODES_BA = 4;
    private static final int NODES_PI = 5;
    private static final int NODES_DM = 6;
    private static final int PATH_BA = 7;
    private static final int PATH_PI = 8;
    private static final int PATH_DM = 9;

    private javax.swing.JTable tblResults;
    private Puzzle puzzle = null;
    private boolean startActive = true;

    private ArrayList<Object[]> results;

    /**
     * Creates new form MainWindow
     */
    public MainWindow() {

        initComponents();
        initTable();
    }

    private void initTable() {

        results = new ArrayList<Object[]>();

        DefaultTableModel dm = new DefaultTableModel();

        dm.setDataVector(null, new Object[] { "N", "BA", "PI", "DM", "BA",
                "PI", "DM", "BA", "PI", "DM" });

        this.tblResults = new JTable(dm) {

            @Override
            protected JTableHeader createDefaultTableHeader() {

                return new GroupableTableHeader(columnModel);
            }
        };

        TableColumnModel cm = tblResults.getColumnModel();

        ColumnGroup a_star;
        ColumnGroup g_time = new ColumnGroup("Tiempo (nanosegundos)");
        a_star = new ColumnGroup("A*");
        g_time.add(cm.getColumn(1));
        a_star.add(cm.getColumn(2));
        a_star.add(cm.getColumn(3));
        g_time.add(a_star);

        ColumnGroup g_nodes = new ColumnGroup("Nodos Expandidos");
        a_star = new ColumnGroup("A*");
        g_nodes.add(cm.getColumn(4));
        a_star.add(cm.getColumn(5));
        a_star.add(cm.getColumn(6));
        g_nodes.add(a_star);

        ColumnGroup g_solution = new ColumnGroup("Longitud Camino");
        a_star = new ColumnGroup("A*");
        g_solution.add(cm.getColumn(7));
        a_star.add(cm.getColumn(8));
        a_star.add(cm.getColumn(9));
        g_solution.add(a_star);

        GroupableTableHeader header = (GroupableTableHeader) tblResults
                .getTableHeader();
        header.addColumnGroup(g_time);
        header.addColumnGroup(g_nodes);
        header.addColumnGroup(g_solution);

        scrllTable.getViewport().add(this.tblResults, null);
        this.tblResults.setEnabled(false);

        checkActions();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed"
    // <editor-fold defaultstate="collapsed"
    // <editor-fold defaultstate="collapsed"
    // desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        scrllTable = new javax.swing.JScrollPane();
        jLabel3 = new javax.swing.JLabel();
        btnStart = new javax.swing.JButton();
        txtNValue = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        cmbAlgorithm = new javax.swing.JComboBox();
        lblHeuristic = new javax.swing.JLabel();
        cmbHeuristic = new javax.swing.JComboBox();
        chkSameBoard = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Parámetros");

        jLabel2.setText("Valor de n:");

        scrllTable.setName("scrllTable"); // NOI18N

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Resultados");

        btnStart.setText("Comenzar");
        btnStart.setToolTipText("");
        btnStart.setEnabled(false);
        btnStart.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                btnStartActionPerformed(evt);
            }
        });

        txtNValue
                .setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(
                        new javax.swing.text.NumberFormatter(
                                java.text.NumberFormat.getIntegerInstance())));
        txtNValue.addKeyListener(new java.awt.event.KeyAdapter() {

            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {

                txtNValueKeyTyped(evt);
            }
        });

        jLabel4.setText("Algoritmo:");

        cmbAlgorithm.setModel(new javax.swing.DefaultComboBoxModel(
                new String[] { "Busqueda en Anchura", "Busqueda A*" }));
        cmbAlgorithm.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                cmbAlgorithmActionPerformed(evt);
            }
        });

        lblHeuristic.setText("Heuristica:");

        cmbHeuristic
                .setModel(new javax.swing.DefaultComboBoxModel(new String[] {
                        "Piezas incorrectas", "Distancia de Manhattan" }));

        chkSameBoard.setText("Utilizar el mismo tablero");
        chkSameBoard.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
                getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout
                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(
                        layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(
                                        layout.createParallelGroup(
                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(scrllTable)
                                                .addComponent(jSeparator1)
                                                .addGroup(
                                                        layout.createSequentialGroup()
                                                                .addComponent(
                                                                        jLabel1)
                                                                .addGap(0,
                                                                        0,
                                                                        Short.MAX_VALUE))
                                                .addGroup(
                                                        layout.createSequentialGroup()
                                                                .addGroup(
                                                                        layout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                .addGroup(
                                                                                        layout.createSequentialGroup()
                                                                                                .addGap(26,
                                                                                                        26,
                                                                                                        26)
                                                                                                .addGroup(
                                                                                                        layout.createParallelGroup(
                                                                                                                javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                                                .addComponent(
                                                                                                                        jLabel4)
                                                                                                                .addComponent(
                                                                                                                        jLabel2)
                                                                                                                .addComponent(
                                                                                                                        lblHeuristic)))
                                                                                .addComponent(
                                                                                        jLabel3))
                                                                .addPreferredGap(
                                                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addGroup(
                                                                        layout.createParallelGroup(
                                                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                                                .addComponent(
                                                                                        txtNValue)
                                                                                .addComponent(
                                                                                        cmbAlgorithm,
                                                                                        0,
                                                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                                        Short.MAX_VALUE)
                                                                                .addComponent(
                                                                                        cmbHeuristic,
                                                                                        0,
                                                                                        235,
                                                                                        Short.MAX_VALUE)
                                                                                .addGroup(
                                                                                        layout.createSequentialGroup()
                                                                                                .addGap(0,
                                                                                                        0,
                                                                                                        Short.MAX_VALUE)
                                                                                                .addComponent(
                                                                                                        chkSameBoard)
                                                                                                .addPreferredGap(
                                                                                                        javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                                                                .addComponent(
                                                                                                        btnStart)))))
                                .addContainerGap()));
        layout.setVerticalGroup(layout
                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(
                        layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel1)
                                .addGap(14, 14, 14)
                                .addComponent(jSeparator1,
                                        javax.swing.GroupLayout.PREFERRED_SIZE,
                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(
                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(
                                        layout.createParallelGroup(
                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jLabel2)
                                                .addComponent(
                                                        txtNValue,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE,
                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(
                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(
                                        layout.createParallelGroup(
                                                javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel4)
                                                .addComponent(
                                                        cmbAlgorithm,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE,
                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(
                                        javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(
                                        layout.createParallelGroup(
                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(lblHeuristic)
                                                .addComponent(
                                                        cmbHeuristic,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE,
                                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(
                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(
                                        layout.createParallelGroup(
                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(btnStart)
                                                .addComponent(jLabel3)
                                                .addComponent(chkSameBoard))
                                .addPreferredGap(
                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scrllTable,
                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                        255, Short.MAX_VALUE).addContainerGap()));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void setPuzzle(Puzzle puzzle) {

        this.puzzle = puzzle;
    }

    public void addResult(Object[] row) {

        DefaultTableModel model = (DefaultTableModel) this.tblResults
                .getModel();
        model.addRow(row);
    }

    public void refreshResults() {

        DefaultTableModel model = (DefaultTableModel) this.tblResults
                .getModel();

        model.setRowCount(0);

        for (Object[] result : results) {
            Object[] newResult = new Object[result.length];

            newResult[SIZE] = String.valueOf(result[SIZE]);

            newResult[TIME_BA] = (Long) result[TIME_BA] >= 0 ? String
                    .valueOf(result[TIME_BA]) : "-";
            newResult[TIME_PI] = (Long) result[TIME_PI] >= 0 ? String
                    .valueOf(result[TIME_PI]) : "-";
            newResult[TIME_DM] = (Long) result[TIME_DM] >= 0 ? String
                    .valueOf(result[TIME_DM]) : "-";

            newResult[NODES_BA] = (Integer) result[NODES_BA] >= 0 ? String
                    .valueOf(result[NODES_BA]) : "-";
            newResult[NODES_PI] = (Integer) result[NODES_PI] >= 0 ? String
                    .valueOf(result[NODES_PI]) : "-";
            newResult[NODES_DM] = (Integer) result[NODES_DM] >= 0 ? String
                    .valueOf(result[NODES_DM]) : "-";

            newResult[PATH_BA] = (Integer) result[PATH_BA] >= 0 ? String
                    .valueOf(result[PATH_BA]) : "-";
            newResult[PATH_PI] = (Integer) result[PATH_PI] >= 0 ? String
                    .valueOf(result[PATH_PI]) : "-";
            newResult[PATH_DM] = (Integer) result[PATH_DM] >= 0 ? String
                    .valueOf(result[PATH_DM]) : "-";

            model.addRow(newResult);
        }
        this.tblResults.repaint();
    }

    public void addBAResult(int size, int expandedNodes, long time, int path) {

        Object[] row = results.get(obtenerResult(size));

        if ((Long) row[TIME_BA] == -1
                || (path >= 0 && (Integer) row[PATH_BA] == -1)
                || time < (Long) row[TIME_BA]) {
            row[TIME_BA] = time;
            row[NODES_BA] = expandedNodes;
            row[PATH_BA] = path;
            refreshResults();
        }

    }

    public void addPIResult(int size, int expandedNodes, long time, int path) {

        Object[] row = results.get(obtenerResult(size));

        if ((Long) row[TIME_PI] == -1
                || (path >= 0 && (Integer) row[PATH_PI] == -1)
                || time < (Long) row[TIME_PI]) {
            row[TIME_PI] = time;
            row[NODES_PI] = expandedNodes;
            row[PATH_PI] = path;
            refreshResults();
        }

    }

    public void addDMResult(int size, int expandedNodes, long time, int path) {

        Object[] row = results.get(obtenerResult(size));

        if ((Long) row[TIME_DM] == -1
                || (path >= 0 && (Integer) row[PATH_DM] == -1)
                || time < (Long) row[TIME_DM]) {
            row[TIME_DM] = time;
            row[NODES_DM] = expandedNodes;
            row[PATH_DM] = path;
            refreshResults();
        }

    }

    private int obtenerResult(int size) {

        int r = -1;
        int c = 0;
        for (Object[] result : results) {
            if ((Integer) (result[0]) == size) {
                r = c;
            }
            c++;
        }
        if (r == -1) {
            results.add(new Object[] { size, -1L, -1L, -1L, -1, -1, -1, -1, -1,
                    -1 });
            r = results.size() - 1;
        }
        return r;
    }

    private void txtNValueKeyTyped(java.awt.event.KeyEvent evt) {// GEN-FIRST:event_txtNValueKeyTyped

        // TODO add your handling code here:

        char c = evt.getKeyChar();
        if (!(Character.isDigit(c) || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
            evt.consume();
        }
        checkActions();

    }// GEN-LAST:event_txtNValueKeyTyped

    private void cmbAlgorithmActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_cmbAlgorithmActionPerformed

        checkActions();
    }// GEN-LAST:event_cmbAlgorithmActionPerformed

    private void checkActions() {

        if (txtNValue.getText() != null && !txtNValue.getText().isEmpty()
                && Integer.parseInt(txtNValue.getText()) > 0 && startActive) {
            btnStart.setEnabled(true);
        } else {
            btnStart.setEnabled(false);
        }
        if (cmbAlgorithm.getSelectedIndex() == 0) {
            lblHeuristic.setVisible(false);
            cmbHeuristic.setVisible(false);
        } else {
            lblHeuristic.setVisible(true);
            cmbHeuristic.setVisible(true);
        }

        if (btnStart.isEnabled() && puzzle != null
                && Integer.parseInt(txtNValue.getText()) == puzzle.getSize()) {
            chkSameBoard.setEnabled(true);
        } else {
            chkSameBoard.setEnabled(false);
        }
    }

    private void btnStartActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnStartActionPerformed

        if (txtNValue.getText() != null && !txtNValue.getText().isEmpty()) {

            SearchType type;

            if (cmbAlgorithm.getSelectedIndex() == 0) {
                type = SearchType.BFS;
            } else {
                type = SearchType.ASTAR;
            }

            Heuristic heuristic;
            if (cmbHeuristic.getSelectedIndex() == 0) {
                heuristic = Heuristic.WRONG_NUMBER;
            } else {
                heuristic = Heuristic.MANHATTAN_DISTANCE;
            }

            if (!chkSameBoard.isSelected() || !chkSameBoard.isEnabled()) {
                puzzle = null;
            }

            PuzzleWindow pw = new PuzzleWindow(this, puzzle,
                    Integer.parseInt(txtNValue.getText()), type, heuristic);
            startActive = false;
            checkActions();
            pw.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            pw.addWindowListener(new WindowAdapter() {

                @Override
                public void windowClosing(WindowEvent we) {

                    startActive = true;

                    checkActions();
                }
            });

            pw.setVisible(true);

        }

    }// GEN-LAST:event_btnStartActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnStart;
    private javax.swing.JCheckBox chkSameBoard;
    private javax.swing.JComboBox cmbAlgorithm;
    private javax.swing.JComboBox cmbHeuristic;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblHeuristic;
    private javax.swing.JScrollPane scrllTable;
    private javax.swing.JFormattedTextField txtNValue;
    // End of variables declaration//GEN-END:variables

}
