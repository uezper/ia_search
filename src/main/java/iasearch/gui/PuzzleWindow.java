/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package iasearch.gui;

import iasearch.puzzle.Position;
import iasearch.puzzle.Puzzle;
import iasearch.search.AStarSearch;
import iasearch.search.AStarSearch.Heuristic;
import iasearch.search.BFSearch;
import iasearch.search.SearchType;
import java.awt.Color;
import java.awt.Component;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * 
 * @author Uriel Pereira
 */
public class PuzzleWindow extends javax.swing.JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JButton[][] botones;
    private Puzzle puzzle;
    private JPanel acciones;
    private JPanel information;
    private JButton btnStart;
    private JButton btnNext;
    private JButton btnPrev;
    private JButton btnFirst;
    private JButton btnLast;

    private JLabel lblExpandedNodes;
    private JLabel lblTime;
    private JLabel lblFoundSolution;

    private BFSearch bfs;
    private AStarSearch astar;

    private boolean runAlgorithm = true;
    private boolean stopAlgorithm = false;

    private SearchType searchType;
    private Heuristic heuristic;
    private MainWindow mw;

    /**
     * Creates new form PuzzleWindow
     */
    public PuzzleWindow() {

        initComponents();
    }

    public PuzzleWindow(MainWindow mw, Puzzle p, int n, SearchType searchType,
            Heuristic heuristic) {

        initComponents();

        this.searchType = searchType;
        this.heuristic = heuristic;
        this.mw = mw;

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setAlignmentX(Component.LEFT_ALIGNMENT);

        if (p == null) {
            puzzle = new Puzzle(n);
        } else {
            puzzle = p;
            p.getInitialState();
            p.cleanFinalState();
        }

        mw.setPuzzle(puzzle);

        acciones = new JPanel();
        acciones.setLayout(new BoxLayout(acciones, BoxLayout.LINE_AXIS));
        acciones.setAlignmentX(Component.LEFT_ALIGNMENT);

        btnStart = new JButton("Correr Algoritmo");
        btnStart.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                btnStartActionPerformed(evt);
            }
        });

        btnFirst = new JButton("Inicial");
        btnFirst.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                btnFirstActionPerformed(evt);
            }
        });

        btnLast = new JButton("Final");
        btnLast.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                btnLastActionPerformed(evt);
            }
        });

        btnNext = new JButton("Siguiente");
        btnNext.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                btnNextActionPerformed(evt);
            }
        });

        btnPrev = new JButton("Anterior");
        btnPrev.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                btnPrevActionPerformed(evt);
            }
        });

        acciones.add(btnStart);
        acciones.add(btnFirst);
        acciones.add(btnPrev);
        acciones.add(btnNext);
        acciones.add(btnLast);

        int size = puzzle.getSize();

        String algorithm;
        if (searchType == SearchType.BFS) {
            algorithm = "Busqueda en anchura";
        } else {
            algorithm = "Busqueda A*";
        }

        String heur = "";
        if (heuristic != null) {
            if (heuristic == Heuristic.WRONG_NUMBER) {
                heur = "Cantidad de piezas incorrectas";
            } else {
                heur = "Distancia de Manhattan";
            }
        }

        JLabel lblNodos = new JLabel("Celdas: " + size * size);
        JLabel lblAlgorithm = new JLabel("Algoritmo: " + algorithm);
        JLabel lblHeuristic = new JLabel("Heuristica: " + heur);

        lblExpandedNodes = new JLabel("Nodos Expandidos: ");
        lblExpandedNodes.setVisible(false);

        lblTime = new JLabel("Tiempo: ");
        lblTime.setVisible(false);

        lblFoundSolution = new JLabel("Solucion encontrada: ");
        lblFoundSolution.setVisible(false);

        information = new JPanel();
        information.setLayout(new BoxLayout(information, BoxLayout.Y_AXIS));
        information.setAlignmentX(Component.LEFT_ALIGNMENT);

        JPanel linePanel;

        linePanel = new JPanel();
        linePanel.setLayout(new BoxLayout(linePanel, BoxLayout.LINE_AXIS));
        linePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        linePanel.add(lblNodos);
        information.add(linePanel);

        linePanel = new JPanel();
        linePanel.setLayout(new BoxLayout(linePanel, BoxLayout.LINE_AXIS));
        linePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        linePanel.add(lblAlgorithm);
        information.add(linePanel);

        if (searchType == SearchType.ASTAR) {
            linePanel = new JPanel();
            linePanel.setLayout(new BoxLayout(linePanel, BoxLayout.LINE_AXIS));
            linePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
            linePanel.add(lblHeuristic);
            information.add(linePanel);
        }

        linePanel = new JPanel();
        linePanel.setLayout(new BoxLayout(linePanel, BoxLayout.LINE_AXIS));
        linePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        linePanel.add(lblTime);
        information.add(linePanel);

        linePanel = new JPanel();
        linePanel.setLayout(new BoxLayout(linePanel, BoxLayout.LINE_AXIS));
        linePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        linePanel.add(lblExpandedNodes);
        information.add(linePanel);

        linePanel = new JPanel();
        linePanel.setLayout(new BoxLayout(linePanel, BoxLayout.LINE_AXIS));
        linePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        linePanel.add(lblFoundSolution);
        information.add(linePanel);

        drawPuzzle();

    }

    private void btnStartActionPerformed(java.awt.event.ActionEvent evt) {

        if (runAlgorithm) {

            runAlgorithm = false;
            stopAlgorithm = true;

            Thread t;
            if (searchType == SearchType.BFS) {
                bfs = new BFSearch(puzzle, this);
                t = new Thread(bfs);
            } else {
                astar = new AStarSearch(puzzle, this, heuristic);
                t = new Thread(astar);
            }

            t.start();
            checkActions();
        } else if (stopAlgorithm) {
            if (searchType == SearchType.BFS) {
                bfs.setFinished(true);
            } else {
                astar.setFinished(true);
            }
            stopAlgorithm = false;
        }

    }

    private void btnFirstActionPerformed(java.awt.event.ActionEvent evt) {

        if (puzzle.hasPrev()) {
            puzzle.getInitialState();
            drawPuzzle();
        }
    }

    private void btnLastActionPerformed(java.awt.event.ActionEvent evt) {

        if (puzzle.hasNext()) {
            puzzle.getFinalState();
            drawPuzzle();
        }
    }

    private void btnNextActionPerformed(java.awt.event.ActionEvent evt) {

        if (puzzle.nextState()) {
            drawPuzzle();
        }
    }

    private void btnPrevActionPerformed(java.awt.event.ActionEvent evt) {

        if (puzzle.prevState()) {
            drawPuzzle();
        }
    }

    public Puzzle getPuzzle() {

        return puzzle;
    }

    /**
     * Dibuja en la ventana el Puzzle
     */
    private void drawPuzzle() {

        panel.removeAll();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));

        int n = puzzle.getSize();
        int[][] puzzle_matrix = puzzle.getPuzzle();
        Position pos = puzzle.getPuzzlePosition();

        botones = new JButton[n][n];

        int pad = Integer.toString(n * n).length();

        for (int i = 0; i < n; i++) {
            JPanel linePanel = new JPanel();
            linePanel.setLayout(new BoxLayout(linePanel, BoxLayout.LINE_AXIS));
            linePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
            for (int j = 0; j < n; j++) {
                botones[i][j] = new JButton(String.format("%0" + pad + "d",
                        puzzle_matrix[i][j]));
                botones[i][j].setEnabled(false);

                if (pos.row == i && pos.col == j) {
                    botones[i][j].setBackground(Color.YELLOW);
                }

                linePanel.add(botones[i][j]);
            }
            panel.add(linePanel);
        }

        panel.add(acciones);
        panel.add(information);
        panel.revalidate();
        panel.repaint();
        pack();

        checkActions();
    }

    private void checkActions() {

        boolean hasPrev = puzzle.hasPrev();
        boolean hasNext = puzzle.hasNext();

        btnPrev.setEnabled(hasPrev);
        btnFirst.setEnabled(hasPrev);
        btnNext.setEnabled(hasNext);
        btnLast.setEnabled(hasNext);

        if (runAlgorithm) {
            btnStart.setEnabled(runAlgorithm);
        } else if (stopAlgorithm) {
            btnStart.setText("Detener");
            btnStart.setEnabled(true);
        } else {
            btnStart.setEnabled(false);
        }
    }

    public void BFSearchFinished() {

        long diff = (bfs.getFinishedTime() - bfs.getStartedTime());
        String found = bfs.isFoundSolution() ? "Si" : "No";

        lblTime.setText(lblTime.getText() + diff + " nanosegundos");
        lblExpandedNodes.setText(lblExpandedNodes.getText()
                + bfs.getNodesExpanded());
        lblFoundSolution.setText(lblFoundSolution.getText() + found);

        lblTime.setVisible(true);
        lblExpandedNodes.setVisible(true);
        lblFoundSolution.setVisible(true);

        puzzle.setMovements(bfs.getPath());
        puzzle.setFinalState();
        puzzle.getInitialState();

        int path = bfs.isFoundSolution() ? bfs.getPath().size() : -1;

        stopAlgorithm = false;
        mw.addBAResult(puzzle.getSize(), bfs.getNodesExpanded(), diff, path);
        drawPuzzle();
    }

    public void AStarFinished() {

        long diff = (astar.getFinishedTime() - astar.getStartedTime());        
        String found = astar.isFoundSolution() ? "Si" : "No";

        lblTime.setText(lblTime.getText() + diff + " nanosegundos");
        lblExpandedNodes.setText(lblExpandedNodes.getText()
                + astar.getNodesExpanded());
        lblFoundSolution.setText(lblFoundSolution.getText() + found);

        lblTime.setVisible(true);
        lblExpandedNodes.setVisible(true);
        lblFoundSolution.setVisible(true);

        puzzle.setMovements(astar.getPath());
        puzzle.setFinalState();
        puzzle.getInitialState();

        stopAlgorithm = false;

        int path = astar.isFoundSolution() ? astar.getPath().size() : -1;

        if (heuristic == Heuristic.WRONG_NUMBER) {
            mw.addPIResult(puzzle.getSize(), astar.getNodesExpanded(), diff,
                    path);
        } else {
            mw.addDMResult(puzzle.getSize(), astar.getNodesExpanded(), diff,
                    path);
        }

        drawPuzzle();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed"
    // <editor-fold defaultstate="collapsed"
    // desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(panelLayout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 380,
                Short.MAX_VALUE));
        panelLayout.setVerticalGroup(panelLayout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 278,
                Short.MAX_VALUE));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
                getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panel,
                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                Short.MAX_VALUE).addContainerGap()));
        layout.setVerticalGroup(layout.createParallelGroup(
                javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(panel,
                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                Short.MAX_VALUE).addContainerGap()));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel panel;
    // End of variables declaration//GEN-END:variables
}
