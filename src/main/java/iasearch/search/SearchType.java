package iasearch.search;

public enum SearchType {
    BFS, ASTAR
}
