/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package iasearch.search;

import iasearch.gui.PuzzleWindow;
import iasearch.puzzle.Movement;
import iasearch.puzzle.Puzzle;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.misc.Queue;

/**
 * 
 * @author Uriel Pereira
 */
public class BFSearch implements Runnable {

    private final Movement[] next_movements = new Movement[] { Movement.DOWN,
            Movement.UP, Movement.LEFT, Movement.RIGHT };

    private final Puzzle puzzle;
    private final int size;
    private boolean finished;
    private int nodesExpanded;
    private long startedTime;
    private long finishedTime;
    private boolean foundSolution;
    private ArrayList<Movement> path;

    private final Queue queue;
    private final PuzzleWindow pw;

    public BFSearch(Puzzle puzzle, PuzzleWindow pw) {

        this.puzzle = puzzle;
        this.pw = pw;
        size = puzzle.getSize();
        finished = false;
        nodesExpanded = 0;
        foundSolution = false;
        queue = new Queue();
    }

    public void run() {

        try {
            startedTime = System.nanoTime();
            State lastNode = doSearch();
            finishedTime = System.nanoTime();
            path = getMovements(lastNode);

            pw.BFSearchFinished();
        } catch (InterruptedException ex) {
            Logger.getLogger(BFSearch.class.getName()).log(Level.SEVERE, null,
                    ex);
        }

    }

    private State doSearch() throws InterruptedException {

        State currentState = null;

        // Cargar estados iniciales
        for (Movement next : next_movements) {
            if (puzzle.isValidMovement(next)) {
                State newNode = new State();
                newNode.parent = currentState;
                newNode.movement = next;
                queue.enqueue(newNode);
            }
        }

        while (!queue.isEmpty()) {
            currentState = (State) queue.dequeue();
            // Si se ha interrumpido la ejecucion se devuelve el ultimo nodo
            // visitado
            if (finished) {
                return currentState;
            }

            // Expandiendo un nuevo nodo
            nodesExpanded++;

            // Establecer el estado correcto del puzzle de acuerdo a los
            // movimientos
            puzzle.getInitialState();
            ArrayList<Movement> movements = getMovements(currentState);
            for (Movement movement : movements) {
                puzzle.move(movement);
            }

            // Verificar que no me encuentre en una solucion
            if (puzzle.isOrdered()) {
                foundSolution = true;
                return currentState;
            }

            // Verificar los proximos estados posibles y agregarlos a la cola
            for (Movement next : next_movements) {
                if (next != Movement.getOpposite(currentState.movement)) {
                    if (puzzle.isValidMovement(next)) {
                        State newNode = new State();
                        newNode.parent = currentState;
                        newNode.movement = next;
                        queue.enqueue(newNode);
                    }
                }
            }

        }
        return null;
    }

    public void setFinished(boolean finished) {

        this.finished = finished;
    }

    private ArrayList<Movement> getMovements(State finalNode) {

        ArrayList<Movement> movements = new ArrayList<Movement>();
        while (finalNode != null && finalNode.movement != null) {
            movements.add(0, finalNode.movement);
            finalNode = finalNode.parent;
        }
        return movements;
    }

    public int getNodesExpanded() {

        return nodesExpanded;
    }

    public long getStartedTime() {

        return startedTime;
    }

    public long getFinishedTime() {

        return finishedTime;
    }

    public boolean isFoundSolution() {

        return foundSolution;
    }

    public ArrayList<Movement> getPath() {

        return path;
    }

}
