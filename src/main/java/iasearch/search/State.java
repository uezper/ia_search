package iasearch.search;

import iasearch.puzzle.Movement;

public class State {

    public State parent;
    public Movement movement;
    public int heuristic;
    public int pathWeight;    
    public int f;
}
