package iasearch.search;

import iasearch.gui.PuzzleWindow;
import iasearch.puzzle.Movement;
import iasearch.puzzle.Puzzle;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class AStarSearch implements Runnable {

    private final Movement[] next_movements = new Movement[] { Movement.DOWN,
            Movement.UP, Movement.LEFT, Movement.RIGHT };
    private final Puzzle puzzle;
    private final Heuristic heuristic;

    private boolean finished;
    private int nodesExpanded;
    private long startedTime;
    private long finishedTime;
    private boolean foundSolution;
    private ArrayList<Movement> path;

    private final PuzzleWindow pw;

    private PriorityQueue<State> priority;

    public AStarSearch(Puzzle puzzle, PuzzleWindow pw, Heuristic heuristic) {

        this.puzzle = puzzle;
        this.pw = pw;
        this.heuristic = heuristic;
        finished = false;
        nodesExpanded = 0;
        foundSolution = false;

        priority = new PriorityQueue<State>(4, new Comparator<State>() {

            public int compare(State arg0, State arg1) {

                if (arg0.f < arg1.f) {
                    return -1;
                } else if (arg0.f > arg1.f) {
                    return 1;
                }
                return 0;
            }

        });
    }

    public void run() {

        startedTime = System.nanoTime();

        State result = doSearch();

        finishedTime = System.nanoTime();
        path = getMovements(result);

        pw.AStarFinished();
    }

    private State doSearch() {

        // Estado inicial
        State currentState = new State();
        currentState.parent = null;
        currentState.movement = null;
        currentState.pathWeight = 0;    
        currentState.heuristic = -1;
                        
        priority.add(currentState);

        // Verificar si no se ha interrumpido la ejecución del algoritmo
        while (!finished && !priority.isEmpty()) {

            // Visitar siguiente estado
            currentState = priority.poll();                        
            
            // Establecer el estado correcto del puzzle de acuerdo a los
            // movimientos
            puzzle.getInitialState();
            ArrayList<Movement> movements = getMovements(currentState);
            for (Movement movement : movements) {
                puzzle.move(movement);
            }

            // Comprobar si es una meta
            if (currentState.heuristic == 0) {
                foundSolution = true;
                return currentState;
            }

            // Expansion de nodos
            nodesExpanded++;

            for (Movement next : next_movements) {
                if (puzzle.isValidMovement(next)
                        && next != Movement.getOpposite(currentState.movement)) {
                    State newState = new State();
                    newState.parent = currentState;
                    newState.movement = next;
                    newState.pathWeight = currentState.pathWeight + 1;

                    // Calculo de heuristica
                    heuristic(newState);
                    
                    newState.f = newState.heuristic + newState.pathWeight;
                    
                    priority.add(newState);                     
                }
            }
            
        }

        return currentState;
    }

    private void heuristic(State state) {

        puzzle.move(state.movement);

        if (heuristic == Heuristic.WRONG_NUMBER) {
            heuristicWrongNumber(state);
        } else {
            heuristicManhattan(state);
        }

        puzzle.move(Movement.getOpposite(state.movement));
    }

    private void heuristicWrongNumber(State state) {

        int c = 1;
        int size = puzzle.getSize();
        int matrix[][] = puzzle.getPuzzle();
        int r = 0;
        
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (matrix[i][j] != c++ && matrix[i][j] != size*size) {
                    r++;
                }
            }
        }

        state.heuristic = r;
    }

    private void heuristicManhattan(State state) {

        int c = 1;
        int size = puzzle.getSize();
        int matrix[][] = puzzle.getPuzzle();
        int r = 0;
        
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (matrix[i][j] != c++ && matrix[i][j] != size*size) {                    
                    int row = matrix[i][j] / size;
                    int col = matrix[i][j] % size;
                    if (col == 0) {
                        row--;
                        col = size - 1;
                    } else {
                        col--;
                    }
                    r += Math.abs(row - i) + Math.abs(col - j);
                }
            }
        }

        state.heuristic = r;
    }

    private ArrayList<Movement> getMovements(State finalNode) {

        ArrayList<Movement> movements = new ArrayList<Movement>();
        while (finalNode != null && finalNode.movement != null) {
            movements.add(0, finalNode.movement);
            finalNode = finalNode.parent;
        }
        return movements;
    }

    public static enum Heuristic {
        WRONG_NUMBER, MANHATTAN_DISTANCE
    }

    public int getNodesExpanded() {

        return nodesExpanded;
    }

    public long getStartedTime() {

        return startedTime;
    }

    public long getFinishedTime() {

        return finishedTime;
    }

    public boolean isFoundSolution() {

        return foundSolution;
    }

    public void setFinished(boolean finished) {

        this.finished = finished;
    }

    public ArrayList<Movement> getPath() {

        return path;
    }

}
