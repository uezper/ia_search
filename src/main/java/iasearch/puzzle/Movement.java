package iasearch.puzzle;

public enum Movement {
    UP, DOWN, LEFT, RIGHT;

    public static Movement getOpposite(Movement movement) {

        if (movement == null) {
            return null;
        }
        switch (movement) {
            case UP:
                return DOWN;
            case DOWN:
                return UP;
            case LEFT:
                return RIGHT;
            case RIGHT:
                return LEFT;
        }
        return null;
    }
}
