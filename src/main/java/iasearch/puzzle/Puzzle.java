package iasearch.puzzle;

import java.util.ArrayList;
import java.util.Random;

public class Puzzle {

    private int[][] puzzle;
    private int[][] initial_state = null;
    private int[][] final_state = null;
    private final Position position;
    private Position initial_pos;
    private Position final_pos;
    private final int size;
    private ArrayList<Movement> movements;
    private int current;

    public Puzzle(int n) {

        size = n;
        position = new Position();
        movements = new ArrayList<Movement>();
        initPuzzle();
    }

    /**
     * Inicializa la matriz del puzzle
     */
    private void initPuzzle() {

        puzzle = new int[size][size];

        int c = 1;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                puzzle[i][j] = c++;
            }
        }

        position.row = size - 1;
        position.col = size - 1;

        // Desordenar
        Random r = new Random();
        Movement[] movements = new Movement[size * size * 2];
        for (int i = 0; i < movements.length; i++) {
            switch (r.nextInt(4)) {
                case 0:
                    movements[i] = Movement.UP;
                    break;
                case 1:
                    movements[i] = Movement.DOWN;
                    break;
                case 2:
                    movements[i] = Movement.LEFT;
                    break;
                case 3:
                    movements[i] = Movement.RIGHT;
                    break;
            }
        }

        for (int i = movements.length - 1; i >= 1; i--) {
            int j = r.nextInt(i + 1);

            Movement temp = movements[i];
            movements[i] = movements[j];
            movements[j] = temp;
        }

        current = -1;
        for (int i = 0; i < movements.length; i++) {
            if (move(movements[i])) {
                current++;
            }
        }

        setInitialState();
    }

    public int[][] getPuzzle() {

        return puzzle;
    }

    public int getSize() {

        return size;
    }

    public Position getPuzzlePosition() {

        return position;
    }

    public void cleanFinalState() {

        final_state = null;
        movements.clear();
    }

    public boolean move(Movement movement) {

        if (isValidMovement(movement)) {
            Position oldPosition = new Position();
            oldPosition.row = position.row;
            oldPosition.col = position.col;
            switch (movement) {
                case UP:
                    position.row -= 1;
                    break;
                case DOWN:
                    position.row += 1;
                    break;
                case LEFT:
                    position.col -= 1;
                    break;
                case RIGHT:
                    position.col += 1;
                    break;
            }
            swapValues(position, oldPosition);
            return true;
        }
        return false;
    }

    public boolean isValidMovement(Movement movement) {

        if (movement == null) {
            return false;
        }

        switch (movement) {
            case UP:
                if (position.row - 1 < 0) {
                    return false;
                }
                break;
            case DOWN:
                if (position.row + 1 >= size) {
                    return false;
                }
                break;
            case LEFT:
                if (position.col - 1 < 0) {
                    return false;
                }
                break;
            case RIGHT:
                if (position.col + 1 >= size) {
                    return false;
                }
                break;
        }
        return true;
    }

    public void swapValues(Position a, Position b) {

        int temp = puzzle[a.row][a.col];
        puzzle[a.row][a.col] = puzzle[b.row][b.col];
        puzzle[b.row][b.col] = temp;
    }

    public ArrayList<Movement> getMovements() {

        return movements;
    }

    public boolean hasNext() {

        return movements.size() != 0 && current < movements.size() - 1;
    }

    public boolean hasPrev() {

        return movements.size() != 0 && current >= 0;
    }

    public boolean nextState() {

        if (hasNext()) {
            current++;
            move(movements.get(current));
            return true;
        }
        return false;
    }

    public boolean prevState() {

        if (hasPrev()) {
            move(Movement.getOpposite(movements.get(current)));
            current--;
            return true;
        }
        return false;
    }

    private void setInitialState() {

        if (initial_state == null) {
            initial_pos = new Position();
            initial_state = new int[size][size];
            int temp = size * size;
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    initial_state[i][j] = puzzle[i][j];
                    if (puzzle[i][j] == temp) {
                        initial_pos.row = i;
                        initial_pos.col = j;
                    }
                }
            }
        }
    }

    public void setFinalState() {

        final_pos = new Position();
        final_state = new int[size][size];
        int temp = size * size;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                final_state[i][j] = puzzle[i][j];
                if (puzzle[i][j] == temp) {
                    final_pos.row = i;
                    final_pos.col = j;
                }
            }
        }

    }

    public void getInitialState() {

        current = -1;
        position.row = initial_pos.row;
        position.col = initial_pos.col;
        copyState(initial_state);
    }

    public void getFinalState() {

        current = movements.size() - 1;
        position.row = final_pos.row;
        position.col = final_pos.col;
        copyState(final_state);
    }

    private void copyState(int[][] mat) {

        puzzle = new int[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                puzzle[i][j] = mat[i][j];
            }
        }
    }

    public boolean isOrdered() {

        int c = 1;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (puzzle[i][j] != c++) {
                    return false;
                }
            }
        }
        return true;
    }

    public void setMovements(ArrayList<Movement> movements) {

        this.movements = movements;
    }
}
